﻿
using System;

namespace homeworkTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите первое число = ");
            int firstvalue = int.Parse(Console.ReadLine());
            Console.Write("Введите второе число = ");
            int number2 = int.Parse(Console.ReadLine());
            Swap(ref firstvalue, ref number2);
        }
        public static void Swap(ref int firstvalue, ref int secondvalue)
        {
            firstvalue = firstvalue + secondvalue;
            secondvalue = firstvalue - secondvalue;
            firstvalue = firstvalue - secondvalue;
            Console.Write($"Первое число = {firstvalue}, второе число = {secondvalue} ");
        }
    }
}