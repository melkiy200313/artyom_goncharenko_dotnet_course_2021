﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите три цифры одной строкой = ");

            int input = int.Parse(Console.ReadLine());
            int num1 = input / 100;
            int num2 = input % 100 / 10;
            int num3 = input % 10;

            if (num1 >= num2)
            {
                if (num1 >= num3)
                {
                    Console.WriteLine("Самая большая цифра - " + num1);
                }
                else
                {
                    Console.WriteLine("Самая большая цифра - " + num3);
                }
            }
            else if (num3 >= num2)
            {
                Console.WriteLine("Самая большая цифра - " + num3);
            }
            else
            {
                Console.WriteLine("Самая большая цифра - " + num2);
            }
        }
    }
}