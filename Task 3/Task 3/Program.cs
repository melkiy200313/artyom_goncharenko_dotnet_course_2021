﻿using System;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 1;

            Console.WriteLine("Введите число: ");
            string input = Console.ReadLine();
            if (Int32.TryParse(input, out num))
            {
                Console.WriteLine("Ваше число - " + num);
            }
            else
            {
                Console.WriteLine("Вы ввели не целое число,попробуйте заново");
            }

        }
    }
}
